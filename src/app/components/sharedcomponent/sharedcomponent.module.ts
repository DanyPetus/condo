import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SidemenuComponent } from '../sidemenu/sidemenu.component'



@NgModule({
  declarations: [SidemenuComponent],
  imports: [ 
    FormsModule,  
    CommonModule,
    IonicModule
  ],
  exports: [SidemenuComponent]
})
export class SharedComponentsModule { }
