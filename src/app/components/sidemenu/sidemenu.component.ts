import { Component, OnInit } from '@angular/core';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';
// SERVICES
import {
  LoginService
} from '../../services/login/login.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {

  constructor(
    private storage: Storage,
    public router: Router,
    private _loginService: LoginService,
  ) { }

  ngOnInit() {}

  goOut(){
    this.storage.clear();
    this.router.navigateByUrl('login');
  }

  goPago(){
    this.router.navigate(['/pago']);
  }

  goReservar(){
    this.router.navigate(['/reservacion']);
  }

  goMensaje(){
    this.router.navigate(['/chat']);
  }

  goAnuncios(){
    this.router.navigate(['/anuncios']);
  }

  goContacto(){
    this.router.navigate(['/contacto']);
  }

  
}
