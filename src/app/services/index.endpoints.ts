export const Info = {
    login: 'validar_login',
    admin: 'datos_administrador',
    noticiasDashboard: 'noticias_dashboard',
    detalleNoticia: 'detalle_noticia',
    reservacionesDashboard: 'reservaciones_dashboard',
    deudaResidencia: 'deuda_residencia',
    pagos: 'reporte_pagos',
    tablonAnuncions: 'tablon_anuncios',
    selectAmenidades: 'select_amenidades',
    reservarAmenidad: 'guardar_amenidad',
    selectAdmins: 'select_administradores',
    enviarMsj: 'nuevo_mensaje',
    getChat: 'mensajes_conversacion',
    saveDevice: 'guardar_device'
  }