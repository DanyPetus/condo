import {
  Observable,
  throwError
} from 'rxjs';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  map
} from 'rxjs/operators';

import {
  Injectable
} from '@angular/core';

import {
  urlProd
} from '../index.url';

import {
  Info
} from '../index.endpoints';

@Injectable({
  providedIn: 'root'
})
export class InformationService {

  constructor(
    public http: HttpClient
  ) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error has been occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  handleSaveDevice(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.saveDevice + '&id_residencia=' + data.residencia  + '&id_token=' + data.token} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleGetAdmin(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log(data);
    let apiUrl = `${urlProd + Info.admin + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleDeuda(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.deudaResidencia + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleNoticias(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.noticiasDashboard + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleDetalleNoticias(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.detalleNoticia + '&id_residencia=' + data.residencia + '&id_noticia=' + data.id_noticia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleEventos(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.reservacionesDashboard + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handlePay(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.pagos + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleSelAmenidad(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.selectAmenidades + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleCrearAmenidad(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.reservarAmenidad + '&id_residencia=' + data.residencia + '&nombre_amenidad=' + data.nombre_amenidad + '&nombre_persona=' + data.nombre_persona + '&fecha_hora=' + data.fecha_hora} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleGetAnuncios(): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.tablonAnuncions} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleGetSelAdmin(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.selectAdmins + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleGetChat(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.getChat + '&id_residencia=' + data.residencia} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  handleSendMsj(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log();
    let apiUrl = `${urlProd + Info.enviarMsj + '&id_residencia=' + data.residencia + '&id_administrador=' + data.id_administrador + '&mensaje=' + data.mensaje + '&fecha_hora=' + data.fecha_hora} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

}
