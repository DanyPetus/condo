import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./residencia/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./residencia/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'pago',
    loadChildren: () => import('./residencia/pago/pago.module').then( m => m.PagoPageModule)
  },
  {
    path: 'reservacion',
    loadChildren: () => import('./residencia/reservacion/reservacion.module').then( m => m.ReservacionPageModule)
  },
  {
    path: 'anuncios',
    loadChildren: () => import('./residencia/anuncios/anuncios.module').then( m => m.AnunciosPageModule)
  },
  {
    path: 'mensaje',
    loadChildren: () => import('./residencia/mensaje/mensaje.module').then( m => m.MensajePageModule)
  },
  {
    path: 'crearmensaje',
    loadChildren: () => import('./residencia/crearmensaje/crearmensaje.module').then( m => m.CrearmensajePageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./residencia/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'detallenhome',
    loadChildren: () => import('./residencia/detallenhome/detallenhome.module').then( m => m.DetallenhomePageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./residencia/contacto/contacto.module').then( m => m.ContactoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
