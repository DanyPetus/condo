import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  typeNetwork: string;
  public user: Object;
  public status: boolean;

  idResidencia: any;

  selAdmins: any;
  chats: any;

  mensaje: string = ""
  idAdmin: any = "";
  fecha = new Date();
  parseo : any;
  fecha_hora: any = "";

  statusMsj: any;

  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
  ) {}

  ngOnInit() {
    let f = new Date();
    console.log(f);
    f.setDate(f.getDate());
    console.log(f);
    this.fecha_hora = Math.floor(f.getTime() / 1000);
    // this.fecha_hora = Math.floor(this.parseo.getTime() / 1000);
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetSelect();
          this.handleGetChats();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetSelect() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleGetSelAdmin(data)
        .subscribe(res => {
          this.selAdmins = res;
          console.log(this.selAdmins);

          this.idAdmin = this.selAdmins[0].id_administrador;

        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetChats() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleGetChat(data)
        .subscribe(res => {
          this.chats = res;
          console.log(this.chats);

        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleSendMensaje() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia,
        'id_administrador': this.idAdmin,
        'mensaje': this.mensaje,
        'fecha_hora': this.fecha_hora
      };
      if (this.mensaje === "") {
        this.alertMsj(
          "El mensaje está vacío",
          "Intenta otra vez."
        );
      } else {
        await this._infoService.handleSendMsj(data)
          .subscribe(res => {
            this.statusMsj = res;
            console.log(this.statusMsj);

            this.mensaje = "";
            this.handleGetChats();

          }, err => {
            console.log(err);
            this.status = false;
            this.alertMsj(
              "Oh, algo salio mal",
              "Intenta otra vez."
            );

          });
      }
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}