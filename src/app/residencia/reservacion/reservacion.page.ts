import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-reservacion',
  templateUrl: './reservacion.page.html',
  styleUrls: ['./reservacion.page.scss'],
})
export class ReservacionPage implements OnInit {
  typeNetwork: string;
  public user: Object;
  public status: boolean;

  idResidencia: any;
  selAmenidades: any;
  seleccion: string = "";
  min_year: any;

  nombre: string = "";
  fecha_hora_min: any;
  fecha: any = "";
  hora: any = ";"
  creada: any;

  parte1 : any;
  parte2 : any;
  parte3 : any;

  fechas : any;

  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
    public toastController: ToastController
  ) {}

  ngOnInit() {
    let hoy = new Date();
    this.fecha_hora_min = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.min_year = hoy.getFullYear();
    console.log(this.fecha_hora_min)

    this.handleGetStorageUser();
  }


  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetSelect();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetSelect() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleSelAmenidad(data)
        .subscribe(res => {
          this.selAmenidades = res;
          console.log(this.selAmenidades);

        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  selectAmenidad(ev) {
    this.seleccion = ev.detail.value;
    console.log(ev.detail.value);
  }

  date(ev) {
    let today = new Date(ev.detail.value);
    this.fechas = today;
    // let fecha = today.split('T')[0];
    // console.log(new Date(today))
    console.log(today)

    // this.fecha = Math.floor(today.getTime() / 1000);
    // console.log(this.fecha);
  }

  async handleCreateAmenity() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia,
        'nombre_amenidad': this.seleccion,
        'nombre_persona': this.nombre,
        'fecha_hora': this.fecha
      };
      if (this.idResidencia === "") {
        this.alertMsj(
          "Oh, algo salio mal",
          "Intenta otra vez."
        );
      } else if (this.seleccion === "") {
        this.alertMsj(
          "Campo seleccion vacío",
          "Intenta otra vez."
        );
      } else if (this.nombre === "") {
        this.alertMsj(
          "Campo nombre vacío",
          "Intenta otra vez."
        );
      } else if (this.fecha === "") {
        this.alertMsj(
          "Campo fecha vacío",
          "Intenta otra vez."
        );
      } else if (this.hora === "") {
        this.alertMsj(
          "Campo hora vacío",
          "Intenta otra vez."
        );
      } else {
        await this._infoService.handleCrearAmenidad(data)
          .subscribe(res => {
            this.creada = res;
            this.router.navigate(['/home']);
            this.presentToast();
            console.log(this.creada);

          }, err => {
            console.log(err);
            this.status = false;
            this.alertMsj(
              "Oh, algo salio mal",
              "Intenta otra vez."
            );

          });
      }
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'La reservacion se ha guardado con éxito',
      duration: 2000
    });
    toast.present();
  }

  hour(ev) {
    let hora = new Date(ev.detail.value);
    let seconds = hora.getSeconds();
    let minutes = hora.getMinutes();
    let hour = hora.getHours();
    this.parte2 = hour + ':' + minutes + ':' + seconds;

    let hoy = new Date(this.fechas);
    let set = hoy.setHours(hour);
    console.log(hoy)

    this.fecha = Math.floor(hoy.getTime() / 1000);
    console.log(this.fecha);

  }

}