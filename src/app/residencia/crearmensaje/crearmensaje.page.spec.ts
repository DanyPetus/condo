import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearmensajePage } from './crearmensaje.page';

describe('CrearmensajePage', () => {
  let component: CrearmensajePage;
  let fixture: ComponentFixture<CrearmensajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearmensajePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearmensajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
