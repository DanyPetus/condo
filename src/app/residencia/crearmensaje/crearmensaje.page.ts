import { Component, OnInit } from '@angular/core';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-crearmensaje',
  templateUrl: './crearmensaje.page.html',
  styleUrls: ['./crearmensaje.page.scss'],
})
export class CrearmensajePage implements OnInit {

  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
  }

  openChat(){
    this.router.navigate(['/chat']);
  }

}
