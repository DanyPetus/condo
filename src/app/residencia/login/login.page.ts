import {
  Component,
  OnInit
} from '@angular/core';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';

// PROVIDERS
import {
  LoginService
} from '../../services/login/login.service';

// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';

import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public user: any;
  public status: boolean;

  // LOGIN PARAMS
  email: string;
  contrasena: string;
  login : any;

  typeNetwork: string;


  constructor(
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _loginService: LoginService,
    private network: Network,
    private storage: Storage,
    public router: Router,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(false, 'first');
  }

  // goToHome() {
  //   this.router.navigate(['/home']);
  // }

  async goToHome() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'email': this.email,
        'password': this.contrasena
      };
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._loginService.handleLogin(data)
        .subscribe(res => {
          this.login = res;
          console.log(this.login);

          if (this.login[0]){
            this.router.navigate(['/home']);
            this.handleLocalStorage();
            loading.dismiss();
          } else {
            loading.dismiss();
            this.alertMsj(
              "El usuario o la contraseña son incorrectos",
              "Por favor, intente de nuevo..."
            );
          }

        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo a sucedido.",
            "Si el error persiste, contacta a tu administrador"
          );
          loading.dismiss();
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  handleLocalStorage() {
    this.storage.set('user', this.login);
    // this.handlePageNav();
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}