import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';

import {
  FCM
} from '@ionic-native/fcm/ngx';
import {
  ToastController
} from '@ionic/angular';
import {
  NgCalendarModule,
  CalendarComponent
} from 'ionic2-calendar';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild(CalendarComponent, null) myCalendar: CalendarComponent;

  titleYear: string;
  eventSource = [];

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  selectedDate = new Date();

  onViewTitleChanged(title) {
    this.titleYear = title;
    console.log(title);
  }

  onEventSelected(event) {
    console.log(event)
    if (event.tipo === 0) {
      // console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
      // Data que enviaremos
      let data = event.id
      let navigationExtras: NavigationExtras = {
        queryParams: {
          // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
          // JSON.stringify(data): Recibe data y la convierte en un JSON
          dataCard: JSON.stringify(data)
        }
      };
      // Ruta de Page, NavigationExtras: Data a enviar
      this.router.navigate(['detallenhome'], navigationExtras);
    } else {

    }
  }

  onTimeSelected(ev) {
    console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
      (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
  }

  onCurrentDateChanged(event: Date) {
    console.log('current date change: ' + event);
  }

  onRangeChanged(ev) {
    console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }

  // Variables a usar
  typeNetwork: string;
  public user: Object;
  public status: boolean;

  token: string;
  idResidencia: string;
  administrador: any;
  noticias: any = [];
  eventos: any = [];
  deudas: any;
  devices: any;

  // Datos del admin
  datos_bancarios: string;
  email_administrador: string;
  nombre_administrador: string;
  nombre_complejo: string;
  telefono_administrador: string;

  constructor(
    private fcm: FCM,
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
    private menu: MenuController
  ) {}

  ionViewWillEnter() {
    this.noticias = [];
    this.eventos = [];
    this.eventSource = [];
    this.openFirst();
    this.handleGetEv();
    this.handleGetStorageUser();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  // ionViewWillLeave() {
  //   this.eventSource = [];
  // }

  ngOnInit() {

    this.fcm.getToken().then(token => {
      this.token = token;
      console.log(this.token);
    });

    this.fcm.requestPushPermissionIOS({
      ios9Support: {
        timeout: 10, // How long it will wait for a decision from the user before returning `false`
        interval: 0.3 // How long between each permission verification
      }
    })

    this.fcm.onNotification().subscribe(data => {
      if (data.wasTapped) {
        this.router.navigate(['/home']);
        console.log("ASMS en segundo plano" + JSON.stringify(data));
      } else {
        this.router.navigate(['/home']);
      };
    });

    this.fcm.onTokenRefresh().subscribe(token => {
      this.token = token;
      console.log(this.token);
    });

  }

  exitButton() {
    // console.log("BOOLEAN" + this.exit)
    if (window.confirm("   ¿Seguro que desea salir? ")) {
      navigator["app"].exitApp();
    }
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleSaveDevices();
          this.handleGetDeuda();
          this.handleGetAdmin();
          // this.handleGetNoticias();
          // this.handleGetEventos();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetEv() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetNoticias();
          this.handleGetEventos();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleSaveDevices() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia,
        'token': this.token
      };
      await this._infoService.handleSaveDevice(data)
        .subscribe(res => {
          this.devices = res;
          console.log(this.devices);


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetAdmin() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleGetAdmin(data)
        .subscribe(res => {
          this.administrador = res;
          // console.log(this.administrador);

          this.datos_bancarios = this.administrador[0].datos_bancarios;
          this.email_administrador = this.administrador[0].email_administrador;
          this.nombre_administrador = this.administrador[0].nombre_administrador;
          this.nombre_complejo = this.administrador[0].nombre_complejo;
          this.telefono_administrador = this.administrador[0].telefono_administrador;


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetDeuda() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleDeuda(data)
        .subscribe(res => {
          this.deudas = res[0].deuda;
          console.log(this.deudas[0]);


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetNoticias() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleNoticias(data)
        .subscribe(res => {
          this.noticias = res;
          console.log(this.noticias);

          this.noticias.forEach(element => {
            let tipo = 0
            let id = element.id_noticia;
            let title = element.titulo_noticia
            let startTime = new Date(element.fecha_noticia * 1000)
            startTime.setDate(startTime.getDate() + 1);
            let endTime = new Date(element.fecha_noticia * 1000)
            endTime.setDate(endTime.getDate() + 1);
            let allDay = true
            let arrayCalendar = {
              tipo,
              id,
              title,
              startTime,
              endTime,
              allDay
            }
            this.eventSource.push(arrayCalendar);
          });
          this.myCalendar.loadEvents();


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetEventos() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleEventos(data)
        .subscribe(res => {
          this.eventos = res;
          console.log(this.eventos);

          this.eventos.forEach(element => {
            let tipo = 1
            let id = element.id_reservacion;
            let title = element.titulo_reservacion
            let startTime = new Date(element.fecha_reservacion * 1000)
            startTime.setDate(startTime.getDate() + 1);
            let endTime = new Date(element.fecha_reservacion * 1000)
            endTime.setDate(endTime.getDate() + 1);
            let allDay = true
            let arrayCalendar2 = {
              tipo,
              id,
              title,
              startTime,
              endTime,
              allDay
            }
            this.eventSource.push(arrayCalendar2);
          });
          this.myCalendar.loadEvents();


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  goOut() {
    this.storage.clear();
    this.router.navigateByUrl('login');
  }

}