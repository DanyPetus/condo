import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';


@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {
  typeNetwork: string;
  public user: Object;
  public status: boolean;

  idResidencia : any;
  pagos : any;

  fechas = [];

  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
  ) { }

  ngOnInit() {
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetPay();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetPay() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        mode: 'ios',
        duration: 2000
      });
      await loading.present();
      await this._infoService.handlePay(data)
        .subscribe(res => {
          this.pagos = res;
          console.log(this.pagos);



          this.pagos.forEach(element => {
            let titulo = element.titulo
            let descripcion = element.descripcion
            let monto = element.monto
            let monto_pagado = element.monto_pagado
            let estatus = element.estatus
            let fecha = new Date(element.fecha_factura * 1000)
            let dia = fecha.getDate(); //Tenemos 2050
            let mes = (fecha.getUTCMonth() + 1); //Tenemos 2050
            let año = fecha.getUTCFullYear(); //Tenemos 2050
  
            let date = dia + '/' + mes + '/' + año;

            let arrayCalendar = {
              titulo,
              descripcion,
              monto,
              monto_pagado,
              estatus,
              date
            }
            
            this.fechas.push(arrayCalendar)
          });
          loading.dismiss();


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );
          loading.dismiss();

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}
